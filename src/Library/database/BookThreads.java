package Library.database;

import java.util.Random;

public class BookThreads implements Runnable {
    private String title;
    private String author;
    private String release_date;
    private int number;
    private int sleep;
    private int rand;

    public BookThreads(String title, String author, String release_date, int sleep, int number) {
        this.title = title;
        this.author = author;
        this.release_date = release_date;
        this.sleep = sleep;
        this.number = number;

        Random random = new Random();
        this.rand = random.nextInt(1000);

    }

    @Override
    public void run() {
        System.out.println("\n\nExecuting with these parameters: title =" + title + "author =" + author +
                "release_date" +release_date +"number =" +
                number + "Sleep =" + sleep + "Rand Num = " + rand + "\n\n");
        for(int count = 1; count < rand; count++) {
            if ( count % number == 0) {
                System.out.println(author + " is Sleeping ");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + author + " is done . \n\n");
    }

    }

