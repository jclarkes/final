package Library.database;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThreads {


    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

       BookThreads ds1 = new BookThreads("EndersGame", "OrsonScottCard", "Jan51985", 25, 1000);
        BookThreads ds2 = new BookThreads("JurrassicPark", "Michael Chricton", "November101990", 20, 500);


        myService.execute(ds1);
        myService.execute(ds2);


        myService.shutdown();
    }
}
