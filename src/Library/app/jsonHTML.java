package Library.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class jsonHTML {
    public static String getHTTPContent(String string) {

        String content = "";
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder StringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                StringBuilder.append(line + "\n");
            }
            content = StringBuilder.toString();

        } catch(Exception e) {
            System.err.println(e.toString());
        }
        return content;
    }

    public static Map getHttpHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return hashmap;
    }

    public static  void main(String[] args) {
        System.out.println(jsonHTML.getHTTPContent("https://calgary.bibliocommons.com/user_dashboard"));

        Map<Integer, List<String>> m = jsonHTML.getHttpHeaders("https://calgary.bibliocommons.com/user_dashboard");

        for (Map.Entry<Integer, List<String>> entry: m.entrySet()) {
            try {
                System.out.println("Key= " + entry.getKey() + entry.getValue());

            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }


    }
}
