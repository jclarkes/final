package Library.app;

public class Library {
    private String title;
    private String author;
    private String release_date;

    private String title2;

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getAuthor2() {
        return author2;
    }

    public void setAuthor2(String author2) {
        this.author2 = author2;
    }

    public String getRelease_date2() {
        return release_date2;
    }

    public void setRelease_date2(String release_date2) {
        this.release_date2 = release_date2;
    }
    public String toString2() {
        return "title2: " + title2 + "author2: " + author2 + "release_date2: " + release_date2;
    }

    private String author2;
    private String release_date2;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
    public String toString() {
        return "title: " + title + "author: " + author + "release_date: " + release_date;
    }
}
